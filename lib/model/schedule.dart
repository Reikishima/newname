import 'package:json_annotation/json_annotation.dart';

part 'schedule.g.dart';


@JsonSerializable()
class ResponseModel {
  ResponseModel({
    required this.data,
    required this.support,
  });
  @JsonKey(name: 'data')
  final Data? data;
  @JsonKey(name: 'support')
  final Support? support;


  factory ResponseModel.fromJson(Map<String, dynamic> json) => _$ResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$ResponseModelToJson(this);
}

@JsonSerializable()
class Data {
  Data({
    required this.id,
    required this.email,
    required this.firstName,
    required this.lastName,
    required this.avatar,
  });
  @JsonKey(name: 'id')
  final int? id;
  @JsonKey(name: 'email')
  final String? email;
  @JsonKey(name: 'first_name')
  final String? firstName;
  @JsonKey(name: 'last_name')
  final String? lastName;
  @JsonKey(name: 'avatar')
  final String? avatar;

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

  Map<String, dynamic> toJson() => _$DataToJson(this);
}


@JsonSerializable()
class Support {
  Support({
    required this.url,
    required this.text,
  });
  @JsonKey(name: 'url')
  final String? url;
  @JsonKey(name: 'text')
  final String? text;


  factory Support.fromJson(Map<String, dynamic> json) => _$SupportFromJson(json);

  Map<String, dynamic> toJson() => _$SupportToJson(this);
}
