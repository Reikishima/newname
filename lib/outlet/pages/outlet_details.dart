import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
// ignore: depend_on_referenced_packages
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_row_column.dart';
import '../../frame/frame_scaffold.dart';
import '../../frame_view/frame_action_wrapper.dart';
import '../../utils/app_colors.dart';
import '../../utils/size_config.dart';
import '../Widget/dialog_container.dart';

class OutletDetails extends StatelessWidget {
  const OutletDetails({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return FrameScaffold(
      colorScaffold: AppColors.darkBackground,
      isImplyLeading: false,
      elevation: 0,
      isUseLeading: false,
      action: _actionWrapper(context),
      view: _wrapAll(context),
    );
  }
}

Widget _actionWrapper(BuildContext context) {
  return FrameActionWrapper(
      title: 'Black Owl Location',
      fontWeight: FontWeight.bold,
      onPressed: () {
        Get.back();
      });
}

/// Widget wrap
Widget _wrapAll(BuildContext context) {
  return SingleChildScrollView(
    child: Container(
      color: AppColors.darkBackground,
      width: SizeConfig.screenWidth,
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(child: imageDetail(context)),
          ResponsiveRowColumnItem(child: openOutlet(context)),
          const ResponsiveRowColumnItem(child: DialogContainer()),
        ],
      ),
    ),
  );
}

/// Widget Image Detail Outlet
Widget imageDetail(BuildContext context) {
  return Container(
    width: SizeConfig.screenWidth,
    height: SizeConfig.vertical(50),
    decoration: BoxDecoration(
      color: AppColors.greenInfo,
      image: const DecorationImage(
          image: AssetImage('assets/images/Black_owl.png'), fit: BoxFit.cover),
    ),
  );
}

/// Widget Open Outlet
Widget openOutlet(BuildContext context) {
  return Container(
    margin: EdgeInsets.symmetric(
        vertical: SizeConfig.vertical(1),
        horizontal: SizeConfig.horizontal(2.5)),
    width: SizeConfig.screenWidth,
    height: SizeConfig.vertical(12),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [_textColumn(context), buttonOutlet(context)],
    ),
  );
}

/// Widget text Column Open Outlet
Widget _textColumn(BuildContext context) {
  return Flexible(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Text(
          'Black Owl PIK',
          style: GoogleFonts.montserrat(
              color: AppColors.whiteBackground,
              fontWeight: FontWeight.bold,
              fontSize: 14),
        ),
        Flexible(
          child: RichText(
            text: TextSpan(
              text: 'Sunday ',
              style: GoogleFonts.montserrat(
                  color: AppColors.whiteBackground,
                  fontSize: 12,
                  fontWeight: FontWeight.bold),
              children: <TextSpan>[
                TextSpan(
                  text: '-11.00 until 04.00',
                  style: GoogleFonts.montserrat(
                      color: AppColors.whiteBackground,
                      fontSize: 12,
                      fontWeight: FontWeight.w300),
                )
              ],
            ),
          ),
        ),
        Flexible(
          child: Container(
            padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.horizontal(3),
            ),
            margin: EdgeInsets.only(
                bottom: SizeConfig.vertical(0.5), top: SizeConfig.vertical(1)),
            decoration: BoxDecoration(
              color: AppColors.lightGold,
              borderRadius: BorderRadius.circular(20),
            ),
            height: SizeConfig.vertical(4),
            child: Text(
              'Open',
              textAlign: TextAlign.center,
              style: GoogleFonts.montserrat(
                  color: AppColors.darkBackground,
                  fontSize: 14,
                  fontStyle: FontStyle.italic),
            ),
          ),
        ),
      ],
    ),
  );
}

/// Widget Button Open Outlet
Widget buttonOutlet(BuildContext context) {
  return SizedBox(
    height: SizeConfig.vertical(6),
    child: TextButton.icon(
      style: TextButton.styleFrom(
          backgroundColor: AppColors.darkBackground,
          foregroundColor: AppColors.darkBackground,
          shape: RoundedRectangleBorder(
            side: BorderSide(width: 1, color: AppColors.lightGold),
            borderRadius: BorderRadius.circular(10),
          )),
      onPressed: () {},
      icon: Icon(
        Icons.call,
        size: 20,
        color: AppColors.lightGold,
      ),
      label: Text(
        '0863536299900',
        style: GoogleFonts.montserrat(color: AppColors.lightGold, fontSize: 12),
      ),
    ),
  );
}
