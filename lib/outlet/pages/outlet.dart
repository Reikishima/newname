import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_row_column.dart';
import 'package:uiprojectbo/frame/frame_scaffold.dart';
import 'package:uiprojectbo/frame_view/frame_action_wrapper.dart';

import '../../utils/app_colors.dart';
import '../../utils/size_config.dart';

import '../Widget/value_outlet.dart';

class Outlet extends StatelessWidget {
  const Outlet({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return FrameScaffold(
      colorScaffold: AppColors.darkBackground,
      elevation: 0,
      action: _actionWrapper(context),
      view: _wrap(context),
    );
  }
}

Widget _actionWrapper(BuildContext context) {
  return FrameActionWrapper(
      title: 'Black Owl Location',
      fontWeight: FontWeight.bold,
      onPressed: () {});
}

Widget _wrap(BuildContext context) {
  return SingleChildScrollView(
    child: Column(
      children: [
        ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.COLUMN,
          children: <ResponsiveRowColumnItem>[
            ResponsiveRowColumnItem(child: wrapAll(context)),
          ],
        )
      ],
    ),
  );
}

Widget wrapAll(BuildContext context) {
  return SizedBox(
    child: Column(
      children: [outlet(context)],
    ),
  );
}

Widget outlet(BuildContext context) {
  return SizedBox(
      width: SizeConfig.screenWidth,
      height: SizeConfig.vertical(88),
      child: const ValueOutlet(),
    );
}
