import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../utils/app_colors.dart';
import '../../utils/size_config.dart';

// ignore: must_be_immutable
class CustomCard extends StatelessWidget {
  CustomCard({
    super.key,
    required this.title,
    required this.image,
    required this.time,
    required this.schedule,
  });

  String title;
  String time;
  String image;
  String schedule;

  // String time;
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: AppColors.itemDark,
      ),
      margin: EdgeInsets.symmetric(
          vertical: SizeConfig.vertical(1),
          horizontal: SizeConfig.horizontal(1.5)),
      width: SizeConfig.screenWidth,
      height: SizeConfig.vertical(15),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: SizeConfig.horizontal(30),
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(10),
                  bottomLeft: Radius.circular(10)),
              image: DecorationImage(
                  image: AssetImage(
                    image,
                  ),
                  fit: BoxFit.cover),
            ),
          ),
          Flexible(
            child: Container(
              margin:
                  EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(1)),
              padding: EdgeInsets.only(
                  left: SizeConfig.horizontal(1), top: SizeConfig.vertical(1)),
              width: SizeConfig.horizontal(65),
              height: SizeConfig.vertical(15),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: GoogleFonts.montserrat(
                        color: AppColors.lightGold,
                        fontWeight: FontWeight.bold,
                        fontSize: 14),
                  ),
                  Flexible(
                    child: SizedBox(
                      width: SizeConfig.horizontal(65),
                      height: SizeConfig.vertical(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            time,
                            style: GoogleFonts.montserrat(
                                color: AppColors.whiteBackground,
                                fontWeight: FontWeight.bold,
                                fontSize: 12),
                          ),
                          Row(
                            children: [
                              Flexible(
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                    vertical: SizeConfig.vertical(0.5),
                                    horizontal: SizeConfig.horizontal(3),
                                  ),
                                  decoration: BoxDecoration(
                                    color: AppColors.lightGold,
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  height: SizeConfig.vertical(3.5),
                                  child: Text(
                                    schedule,
                                    textAlign: TextAlign.center,
                                    style: GoogleFonts.montserrat(
                                        color: AppColors.darkBackground,
                                        fontSize: 12,
                                        fontStyle: FontStyle.italic),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
