import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../utils/app_colors.dart';
import '../../utils/size_config.dart';

class DialogContainer extends StatelessWidget {
  const DialogContainer({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      padding: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(2.5)),
      margin: EdgeInsets.symmetric(
          vertical: SizeConfig.vertical(2),
          horizontal: SizeConfig.horizontal(2.5)),
      height: SizeConfig.vertical(50),
      decoration: BoxDecoration(
          color: AppColors.lightGold, borderRadius: BorderRadius.circular(10)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Operational Hours',
            style: GoogleFonts.montserrat(
                color: AppColors.darkBackground,
                fontSize: 14,
                fontWeight: FontWeight.bold),
          ),
          TableDialog(),
          _wrapTextandMap(context)
        ],
      ),
    );
  }
}

class TableDialog extends StatefulWidget {
  const TableDialog({super.key});

  @override
  State<TableDialog> createState() => _TableDialogState();
}

class _TableDialogState extends State<TableDialog> {
  List _items = [];

  Future<void> readJson() async {
    final String response = await rootBundle.loadString('assets/data.json');
    final data = await json.decode(response);
    setState(() {
      _items = data["week"];
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      builder: (context, snapshot) {
        return Table(
          border: TableBorder.all(
              borderRadius: BorderRadius.circular(10), width: 1.5),
          columnWidths: const <int, TableColumnWidth>{
            0: FlexColumnWidth(1.5),
          },
          children: [
            for (var a = 0; a < 7; a++)
              // for (var item in list)
              //   for (var items in hours)
              TableRow(
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                        left: SizeConfig.horizontal(2),
                        top: SizeConfig.vertical(0.5),
                        bottom: SizeConfig.vertical(1)),
                    child: Text(
                      '${snapshot.data}',
                      style: GoogleFonts.montserrat(
                          color: AppColors.darkBackground,
                          fontSize: 12,
                          fontWeight: FontWeight.w300),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: Text(
                     '${snapshot.data}',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.montserrat(
                          color: AppColors.darkBackground,
                          fontSize: 12,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
          ],
        );
      },
    );
  }
}

// /// Widget table Dialog with table
// Widget _dialogTable(BuildContext context) {
//   // var list = [
//   //   'Sunday',
//   //   'Monday',
//   //   'Tuesday',
//   //   'Wednesday',
//   //   'Thursday',
//   //   'Friday',
//   //   'Saturday',
//   // ];
//   // var hours = [
//   //   '11.00 until 04.00',
//   // ];
//   return FutureBuilder(
//     builder: (context, snapshot) {
//       return Table(
//         border: TableBorder.all(
//             borderRadius: BorderRadius.circular(10), width: 1.5),
//         columnWidths: const <int, TableColumnWidth>{
//           0: FlexColumnWidth(1.5),
//         },
//         children: [
//           for (var a = 0; a < 7; a++)
//             // for (var item in list)
//             //   for (var items in hours)
//             TableRow(
//               children: [
//                 Padding(
//                   padding: EdgeInsets.only(
//                       left: SizeConfig.horizontal(2),
//                       top: SizeConfig.vertical(0.5),
//                       bottom: SizeConfig.vertical(1)),
//                   child: Text(
//                     ' item',
//                     style: GoogleFonts.montserrat(
//                         color: AppColors.darkBackground,
//                         fontSize: 12,
//                         fontWeight: FontWeight.w300),
//                   ),
//                 ),
//                 Padding(
//                   padding: const EdgeInsets.all(3.0),
//                   child: Text(
//                     'items',
//                     textAlign: TextAlign.center,
//                     style: GoogleFonts.montserrat(
//                         color: AppColors.darkBackground,
//                         fontSize: 12,
//                         fontWeight: FontWeight.bold),
//                   ),
//                 ),
//               ],
//             ),
//         ],
//       );
//     },
//   );
// }

/// Widget wrap Address and textAddress
Widget _wrapTextandMap(BuildContext context) {
  return SizedBox(
    width: SizeConfig.horizontal(90),
    height: SizeConfig.vertical(15),
    child: Column(
      children: [_addressMap(context), _textAddress(context)],
    ),
  );
}

/// Widget row Address on Dialog
Widget _addressMap(BuildContext context) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Text(
        'Address',
        style: GoogleFonts.montserrat(
            color: AppColors.darkBackground,
            fontSize: 14,
            fontWeight: FontWeight.bold),
      ),
      SizedBox(
        width: SizeConfig.horizontal(27),
        height: SizeConfig.vertical(5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            InkWell(
              onTap: () {},
              child: Text(
                'View in Map',
                style: GoogleFonts.montserrat(
                    decoration: TextDecoration.underline,
                    decorationColor: AppColors.darkBackground,
                    decorationStyle: TextDecorationStyle.solid,
                    decorationThickness: 2,
                    color: AppColors.darkBackground,
                    fontSize: 12,
                    fontWeight: FontWeight.w300),
              ),
            ),
            Icon(
              Icons.location_on,
              color: AppColors.darkBackground,
            )
          ],
        ),
      )
    ],
  );
}

/// Widget Text Address Dialog
Widget _textAddress(BuildContext context) {
  return Row(
    children: [
      Flexible(
        child: SizedBox(
          width: SizeConfig.horizontal(85),
          height: SizeConfig.vertical(10),
          child: Text(
            'Lorem ipsum dolor sit amet, auctor turpis sed, vulputate turpis. Ut vel faucibus neque. Aenean elementum egestas tempor. Integer et tincidunt urna.',
            maxLines: 4,
            style: TextStyle(
              color: AppColors.darkBackground,
              fontWeight: FontWeight.w300,
            ),
          ),
        ),
      )
    ],
  );
}
