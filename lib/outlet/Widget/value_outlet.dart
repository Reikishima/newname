import 'dart:convert';

import 'package:flutter/material.dart';

// ignore: depend_on_referenced_packages
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

import '../../model/schedule.dart';
import '../../utils/app_colors.dart';
import '../../utils/size_config.dart';
import '../pages/outlet_details.dart';
import 'custom_card.dart';

class ValueOutlet extends StatelessWidget {
  const ValueOutlet({Key? key}) : super(key: key);

  Future<ResponseModel?> getDataUser() async {
    Uri url = Uri.parse("https://reqres.in/api/users/2");
    var response = await http.get(url);

    // print(response.statusCode);
    if (response.statusCode == 200) {
      // print(response.body);
      Map<String, dynamic> data =
          (json.decode(response.body) as Map<String, dynamic>);
      return ResponseModel.fromJson(data);
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return FutureBuilder<ResponseModel?>(
      future: getDataUser(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return  Center(child: CircularProgressIndicator(
            color: AppColors.darkBackground,
          ));
        } else {
          if (snapshot.hasData) {
            return ListView(children: [
              for (var a = 0; a < 2; a++)
                InkWell(
                  splashColor: AppColors.darkBackground,
                  highlightColor: Colors.transparent,
                  onTap: () {
                    Get.to(const OutletDetails());
                  },
                  child: CustomCard(
                      title: '${snapshot.data!.data!.firstName}',
                      image: 'assets/images/Black_owl.png',
                      time: '${snapshot.data!.data!.email}',
                      schedule: 'Open'),
                ),
            ]);
          } else {
            return const Center(child: Text("TIDAK ADA DATA"));
          }
        }
      },
    );
  }
}
// for (var widget in list)
//                   InkWell(
//                       splashColor: AppColors.darkBackground,
//                       highlightColor: Colors.transparent,
//                       onTap: () {
//                         Get.to(const OutletDetails());
//                       },
//                       child: widget),